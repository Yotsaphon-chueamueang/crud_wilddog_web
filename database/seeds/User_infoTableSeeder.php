<?php

use Illuminate\Database\Seeder;
use App\User_info;

class User_infoTableSeeder extends Seeder
{

    public function run()
    {
        User_info::create([
            'user_id'                   => '1',
            'first_name'                => 'Yotsaphon',
            'last_name'                 => 'Chueamueangphan',
            'phone_numb'                => '0933100909',
            'address'                   => '19/2 asd Bangkok',
            'company_name'              =>  'Digio',
            'salary'                    =>  '50000',
            // 'image'                   => '638233638.jpg',
            // 'remember_token'        => Str::random(10),
            ]);
    }
}
