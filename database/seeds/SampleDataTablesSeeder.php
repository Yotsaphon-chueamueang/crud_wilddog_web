<?php

use Illuminate\Database\Seeder;
use App\Sample_data;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SampleDataTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sample_data::create([
            'user_name'             => 'yot',
            'email'                 => 'yot@gmail.com',
            'password'              => '123456',
            'first_name'            => 'yot',
            'last_name'             => 'yot',
            // 'remember_token'        => Str::random(10),
            ]);
    }
}
