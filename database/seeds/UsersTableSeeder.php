<?php

use Illuminate\Database\Seeder;
use App\User as Users;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::create([
            'email'                 => 'yot@gmail.com',
            'password'              => Crypt::encrypt('123'),
            'status'              => '1',
            'role_id'              => '1',
            // 'remember_token'        => Str::random(10),
            ]);
    }
}
