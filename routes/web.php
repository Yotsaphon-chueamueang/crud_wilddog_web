<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Spatie\Activitylog\Models\Activity;

Route::get('/', function () {
    // return Activity::all();
    return view('Auth/login');
});
Route::group(['middleware' => 'checkuser'],function(){

Route::resource('sample', 'SampleController');
Route::post('sample/update', 'SampleController@update')->name('sample.update');

Route::get('/index', 'SampleController@index');
Route::get('sample/edit/{id}', 'SampleController@edit');
Route::get('sample/show/{id}', 'SampleController@show');
Route::get('sample/destroy/{id}', 'SampleController@destroy');
});
// login 

// Route::post('/main ', 'LoginController@index');
Route::post('/main/checklogin', 'LoginController@checklogin');
Route::get('main/successlogin ', 'LoginController@successlogin');
Route::get('main/logout ', 'LoginController@logout');

/*
 * Export Buy Record Excel
 */
Route::get('Userexport', 'ExportController@export')->name('Userexport');
Route::get('UserimportExportView', 'ExportController@importExportView');
Route::post('Userimport', 'ExportController@import')->name('Userimport');


// Test email
Route::get('email-test', array('as' => 'normindex', function(){
  
	$details['email'] = 'from@example.com';
  
    dispatch(new App\Jobs\SendEmailJob($details));
  
    return redirect('index');
}));


// test 
// Route::get('/index', function () {
//     return view('WD/index');
// });
Route::get('/login', function () {
    return view('Auth/login');
});
Route::get('/register', function () {
    return view('Auth/register');
});
Route::get('/pass', function () {
    return view('Auth/pass');
});