git clone ลงมา

เปิด terminal พิมพ์
composer install

สร้าง schema database 
ชื่อ testing	utf8 general ci

เปิด terminal พิมพ์
cp .env.example .env
จะมีไฟล .env

แก้ไขไฟล์ .env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=testing
DB_USERNAME=root
DB_PASSWORD=
กด save

เปิด terminal พิมพ์
php artisan key:generate
php artisan migrate:refresh --seed

Run ด้วย
php artisan serve
และเปิด new terminal Run
php artisan queue:listen 
ถ้าไม่ขึ้น copy URL ที่ขึ้นมา <http://127.0.0.1:8000>

ต่อด้วยเปิด CRUD_API

login
yot@gmail.com
123
