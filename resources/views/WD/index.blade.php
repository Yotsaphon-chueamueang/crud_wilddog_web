<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Tables - SB Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand">Start Bootstrap</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button><!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
        <!-- Navbar-->
        <ul class="navbar-nav ml-auto ml-md-0">
            <!-- if(isset(Auth::user()->email)) -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><strong>Welcome </strong><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" >Settings</a><a class="dropdown-item" >Activity Log</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('/main/logout')}}">Logout</a>
                </div>
            </li>
            <!-- else
            <script>
                window.location = "/main";
            </script>
            endif -->
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="layout-static.html">Static Navigation</a><a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a></nav>
                        </div>
                        <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                            <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                            Pages
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a> -->
                        <!-- <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">Authentication
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                                <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                    <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="login.html">Login</a><a class="nav-link" href="register.html">Register</a><a class="nav-link" href="password.html">Forgot Password</a></nav>
                                </div>
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">Error
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                                <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                    <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="401.html">401 Page</a><a class="nav-link" href="404.html">404 Page</a><a class="nav-link" href="500.html">500 Page</a></nav>
                                </div>
                            </nav>
                        </div> -->
                        <div class="sb-sidenav-menu-heading">Addons</div>
                        <a class="nav-link" href="tables.html">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Tables
                        </a>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as:</div>
                    Start Bootstrap
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Tables</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Tables</li>
                    </ol>
                    <!-- <div class="card mb-4">
                            <div class="card-body">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>.</div>
                        </div> -->
                    <div class="card mb-4">
                        <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Example</div>
                        <div class="card-body">
                            <div align="right">
                                <form action="{{ route('Userimport') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <a class="btn btn-warning btn-sm" href="{{ route('Userexport') }}">Export Rec Data</a>
                                </form><br>
                                <a class="btn btn-warning btn-sm" href="{{ route('normindex') }}">Export Rec Email</a>
                                <br>
                                <br>
                                <button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm">Create Record</button>
                            </div>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="user_table" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="5%">id</th>
                                            <th width="15%">Email</th>
                                            <th width="15%">First Name</th>
                                            <th width="15%">Last Name</th>
                                            <th width="15%">Tel</th>
                                            <th width="15%">Address</th>
                                            <th width="15%">Company Name</th>
                                            <th width="15%">Salary</th>
                                            <th width="30%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="5%">id</th>
                                            <th width="15%">Email</th>
                                            <th width="15%">First Name</th>
                                            <th width="15%">Last Name</th>
                                            <th width="15%">Tel</th>
                                            <th width="15%">Address</th>
                                            <th width="15%">Company Name</th>
                                            <th width="15%">Salary</th>
                                            <th width="30%">Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2019</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('assets/js/scripts.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>

<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 65rem">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New User</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-md-4">Email: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="email" name="email" id="email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">password: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="password" name="password" id="password" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">First Name : </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="text" name="first_name" id="first_name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Last Name : </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="text" name="last_name" id="last_name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Tel: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="tel" name="tel" id="tel" class="form-control" pattern="[0-9]{3}-[0-9]{7}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Address: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="text" name="address" id="address" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Company name: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="text" name="company_name" id="company_name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Salary: </label>
                        <div class="col-md-8" style="max-width: 100%">
                            <input type="text" name="salary" id="salary" class="form-control" />
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label >User role: </label>
                        <select id="User_role">
                            <option value="1">Admin</option>
                            <option value="2">Customer</option>
                        </select>
                    </div> -->
                    <br />
                    <div class="form-group" align="center">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Confirmation</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

        $('#user_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('sample.index') }}",
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'first_name',
                    name: 'first_name'
                },
                {
                    data: 'last_name',
                    name: 'last_name'
                },
                {
                    data: 'phone_numb',
                    name: 'phone_numb'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'company_name',
                    name: 'company_name'
                },
                {
                    data: 'salary',
                    name: 'salary'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });



        $('#create_record').click(function() {
            document.getElementById("first_name").readOnly = false;
            document.getElementById("last_name").readOnly = false;
            // document.getElementById("user_name").readOnly = false;
            document.getElementById("password").readOnly = false;
            document.getElementById("email").readOnly = false;
            document.getElementById("tel").readOnly = false;
            document.getElementById("address").readOnly = false;
            document.getElementById("company_name").readOnly = false;
            document.getElementById("salary").readOnly = false;
            document.getElementById("action_button").style.display = "inline";
            $('.modal-title').text('Add New Record');
            $('#action_button').val('Add');
            $('#action').val('Add');
            $('#form_result').html('');
            $('#formModal').modal('show');
        });

        $('#sample_form').on('submit', function(event) {
            event.preventDefault();
            var action_url = '';

            if ($('#action').val() == 'Add') {
                // document.getElementById("action_button").display = block;
                action_url = "{{ route('sample.store') }}";
            }

            if ($('#action').val() == 'Edit') {
                // document.getElementById("action_button").display = block;
                action_url = "{{ route('sample.update') }}";
            }
            // else{document.getElementById("action_button").display = none;

            // }
            $.ajax({
                url: action_url,
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    var html = '';
                    if (data.errors) {
                        html = '<div class="alert alert-danger">';
                        for (var count = 0; count < data.errors.length; count++) {
                            html += '<p>' + data.errors[count] + '</p>';
                        }
                        html += '</div>';
                    }
                    if (data.success) {
                        html = '<div class="alert alert-success">' + data.success + '</div>';
                        $('#sample_form')[0].reset();
                        $('#user_table').DataTable().ajax.reload();
                    }
                    $('#form_result').html(html);
                }
            });
        });

        $(document).on('click', '.edit', function() {
            document.getElementById("first_name").readOnly = false;
            document.getElementById("last_name").readOnly = false;
            // document.getElementById("user_name").readOnly = false;
            document.getElementById("password").readOnly = false;
            document.getElementById("email").readOnly = false;
            document.getElementById("tel").readOnly = false;
            document.getElementById("address").readOnly = false;
            document.getElementById("company_name").readOnly = false;
            document.getElementById("salary").readOnly = false;
            document.getElementById("action_button").style.display = "inline";
            var id = $(this).attr('id');
            $('#form_result').html('');
            $.ajax({
                url: "/sample/edit/" + id,
                dataType: "json",
                success: function(data) {
                    $('#first_name').val(data.result.first_name);
                    $('#last_name').val(data.result.last_name);
                    // $('#user_name').val(data.result.user_name);
                    $('#password').val(data.result.password);
                    $('#email').val(data.result.email);
                    $('#tel').val(data.result.phone_numb);
                    $('#address').val(data.result.address);
                    $('#company_name').val(data.result.company_name);
                    $('#salary').val(data.result.salary);
                    $('#hidden_id').val(id);
                    $('.modal-title').text('Edit Record');
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                    $('#formModal').modal('show');
                }
            })
        });

        $(document).on('click', '.detail', function() {
            document.getElementById("action_button").style.display = "none";
            // document.getElementById("action").style.display = "none";
            document.getElementById("first_name").readOnly = true;
            document.getElementById("last_name").readOnly = true;
            document.getElementById("password").readOnly = true;
            document.getElementById("email").readOnly = true;
            document.getElementById("tel").readOnly = true;
            document.getElementById("address").readOnly = true;
            document.getElementById("company_name").readOnly = true;
            document.getElementById("salary").readOnly = true;
            var id = $(this).attr('id');
            $('#form_result').html('');
            $.ajax({
                url: "/sample/show/" + id,
                dataType: "json",
                success: function(data) {
                    $('#first_name').val(data.result.first_name);
                    $('#last_name').val(data.result.last_name);
                    // $('#user_name').val(data.result.user_name);
                    $('#password').val(data.result.password);
                    $('#email').val(data.result.email);
                    $('#tel').val(data.result.phone_numb);
                    $('#address').val(data.result.address);
                    $('#company_name').val(data.result.company_name);
                    $('#salary').val(data.result.salary);
                    $('#hidden_id').val(id);
                    $('.modal-title').text('View Record');
                    $('#formModal').modal('show');
                }
            })
        });

        var user_id;

        $(document).on('click', '.delete', function() {
            user_id = $(this).attr('id');
            $('#confirmModal').modal('show');
        });

        $('#ok_button').click(function() {
            $.ajax({
                url: "sample/destroy/" + user_id,
                beforeSend: function() {
                    $('#ok_button').text('Deleting...');
                },
                success: function(data) {
                    setTimeout(function() {
                        $('#confirmModal').modal('hide');
                        $('#user_table').DataTable().ajax.reload();
                        alert('Data Deleted');
                    }, 2000);
                }
            })
        });

    });
</script>