<?php

namespace App\Mail;

use App\Exports\UserRecExport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class SentMailxlsx extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build()
    {   
        Excel::store(new UserRecExport(), 'Export/fileName.xlsx', 'local');
        return $this->from('from@example.com', 'Mailtrap')
            ->subject('Test Queued Email')
            ->view('mails.email')
            ->attach('storage\app\Export\fileName.xlsx');
    }
}

