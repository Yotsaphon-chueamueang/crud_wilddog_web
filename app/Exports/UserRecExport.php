<?php

namespace App\Exports;

use App\User_info;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class UserRecExport implements FromCollection, WithHeadings, ShouldAutoSize,  WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User_info::all();
    }
    public function headings(): array
    {
        // return SellRecordProduct::all();
        return [
            '#',
            'First name',
            'Last name',
            'Phone number',
            'Address',
            'Date'
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(18);
            },
        ];
    }
}
