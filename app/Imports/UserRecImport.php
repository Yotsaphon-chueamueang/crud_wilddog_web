<?php

namespace App\Imports;

use App\User_info;
use Maatwebsite\Excel\Concerns\ToModel;

class UserRecImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User_info([
            'id' => $row[0],
            'first_name' => $row[1],
            'last_name' => $row[2],
            'phone_numb' => $row[3],
            'address' => $row[4],
            'created_at' => $row[5]
        ]);
    }
}
