<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserRecExport;

class ExportController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new UserRecExport, 'BuyRecord-'. date('Y-m-d ') .'Time'.date(' H-i-s').'.xlsx');
    }
    

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new UserRecExport,request()->file('file'));

        return back();
    }
}
