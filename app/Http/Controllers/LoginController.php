<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    private $api_client;

    public function __construct()
    {
        $this->api_client             = parent::APIClient();
        $this->API_URL              = env('API_URL');
    }
    function index_login()
    {
        return view('WD/login');
    }
    public function checklogin(Request $request)
    {
        Log::info("emaildb:");
        $email_DB = $request->email;
        $user_data = array(
            'email'  => $request->email,
            'password' => $request->password
        );
        $data = [
            'json' => $user_data
        ];
        $data_sent = $this->api_client->post('/sample/checklogin', $data);
        $data_get=$data_sent->getBody()->getContents();
        Log::info("email:".$request->email);
        Log::info("emaildb:".$email_DB);
        // return response()->json(['success' => 'Data is successfully updated']);

        if ($data_get == true) {
            
            $request->session()->put('email',$request->email);
            return redirect('main/successlogin');
        } elseif($data_get != true) {
            return back()->with('error', 'Wrong Login Details');
        }
    }
    function successlogin()
    {   
        return redirect('index');
    }
    function logout(Request $request)
    {
        // $data_sent = $this->api_client->post('/logout', []);
        $request->session()->flush();
        return redirect('/');
    }
}
