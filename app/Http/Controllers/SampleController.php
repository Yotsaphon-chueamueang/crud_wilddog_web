<?php

namespace App\Http\Controllers;

use App\Sample_data;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
// use GuzzleHttp\Exception\ClientException;
// use Validator;

class SampleController extends Controller
{
    private $api_client;

    public function __construct()
    {
        $this->api_client             = parent::APIClient();
        $this->API_URL              = env('API_URL');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data_sent = $this->api_client->post('/users', []);
            $data = json_decode($data_sent->getBody()->getContents());

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Edit</button>';
                    $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Delete</button>';
                    $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="detail" id="' . $data->id . '" class="detail btn btn-primary btn-sm">Detail</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('WD/index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = array(
            'first_name'        =>  'required',
            'last_name'         =>  'required',
            // 'user_name'     =>  'required',
            'password'          =>  'required',
            'email'             =>  'required',
            'tel'               =>  'required',
            'address'           =>  'required',
            'company_name'      =>  'required',
            'salary'            =>  'required',
            // 'role_id'           =>  'required',
            // 'image'         =>  'required|image|max:2048'
        );

        $error = Validator::make($request->all(), $rules);


        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        // $image = $request->file('image');
        // Log::info($image);
        
        // $new_name = rand() . '.' . $image->getClientOriginalExtension();

        // $image->move(public_path('images'), $new_name);

        $form_data = array(
            'first_name'         =>  $request->first_name,
            'last_name'          =>  $request->last_name,
            // 'user_name'         =>  $request->user_name,
            'password'           =>  $request->password,
            'email'              =>  $request->email,
            'tel'                =>  $request->tel,
            'address'            =>  $request->address,
            'company_name'       =>  $request->company_name,
            'salary'             =>  $request->salary,
            // 'role_id'            =>  $request->User_role,
            // 'image'             =>  $new_name,
        );
        Log::info($form_data);
        $data = [
            'json' => $form_data
        ];

        $data_sent = $this->api_client->post('/store', $data);
        $activity = Activity::all()->last();
        return response()->json(['success' => 'Data Added successfully.']);
    }

    public function show($id)
    {
        if (request()->ajax()) {

            $newid = array(
                'id' => $id
            );

            $id_sent = [
                'json' => $newid
            ];
            $data = $this->api_client->post('/show', $id_sent);
            $data_decode = json_decode($data->getBody()->getContents());
            Log::info(json_encode($data_decode));
            // Log::info($data);
            return response()->json(['result' => $data_decode]);
        }
    }

    public function edit($id)
    {
        if (request()->ajax()) {

            // $data = Sample_data::findOrFail($id);
            Log::info("ID   ");
            Log::info($id);
            $newid = array(
                'id' => $id
            );

            $id_sent = [
                'json' => $newid
            ];

            $data = $this->api_client->post('/edit', $id_sent);
            $data_decode = json_decode($data->getBody()->getContents());
            Log::info(json_encode($data_decode));
            // Log::info($data);
            return response()->json(['result' => $data_decode]);
        }
    }

    public function update(Request $request)
    {
        $rules = array(
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            // 'user_name'     =>  'required',
            'password'      =>  'required',
            'email'         =>  'required',
            'tel'         =>  'required',
            'address'         =>  'required',
            'hidden_id'     =>  'required',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'first_name'        =>  $request->first_name,
            'last_name'         =>  $request->last_name,
            // 'user_name'         =>  $request->user_name,
            'password'          =>  $request->password,
            'email'             =>  $request->email,
            'tel'             =>  $request->tel,
            'address'             =>  $request->address,
            'hidden_id'         =>  $request->hidden_id,

        );

        $data = [
            'json' => $form_data
        ];

        Log::info($data);


        // Sample_data::whereId($request->hidden_id)->update($form_data);
        $data_sent = $this->api_client->post('/update', $data);

        return response()->json(['success' => 'Data is successfully updated']);
    }

    public function destroy($id)
    {
        Log::info("ID   ");
        Log::info($id);
        $newid = array(
            'id' => $id
        );

        $id_sent = [
            'json' => $newid
        ];

        $data = $this->api_client->post('/delete', $id_sent);
        // $data = Sample_data::findOrFail($id);
    }
}
