<?php

namespace App;

use App\User_info as user_data;
use Illuminate\Database\Eloquent\Model;

class User_info extends Model
{
    protected $fillable = [
        'first_name', 
        'last_name',
        'phone_numb',
        'address',
        'company_name',
        'salary',
        // 'image'
       ];
}
